from .unit import StatBlock


class Equipment():
    def __init__(self):
        self.statblock = StatBlock()
    
    
class Unarmed(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.statblock.name = "Unarmed"


class Light(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.statblock.name = "Light"
        self.statblock.power = 1
        self.statblock.defense = 1


class Medium(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.statblock.name = "Medium"
        self.statblock.power = 2
        self.statblock.defense = 2


class Heavy(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.statblock.name = "Heavy"
        self.statblock.power = 4
        self.statblock.defense = 4


class SuperHeavy(Equipment):
    def __init__(self):
        Equipment.__init__(self)
        self.statblock.name = "Super-Heavy"
        self.statblock.power = 6
        self.statblock.defense = 6
