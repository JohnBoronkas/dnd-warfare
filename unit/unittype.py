from .trait import Charge
from .unit import StatBlock


class UnitType():
    def __init__(self):
        self.statblock = StatBlock()
        self.costmodifier = 1


class Flying(UnitType):
    def __init__(self):
        UnitType.__init__(self)
        self.statblock.name = "Flying"
        self.statblock.morale = 3
        self.costmodifier = 2


class Archers(UnitType):
    def __init__(self):
        UnitType.__init__(self)
        self.statblock.name = "Archers"
        self.statblock.power = 1
        self.statblock.morale = 1
        self.costmodifier = 1.75


class Cavalry(UnitType):
    def __init__(self):
        UnitType.__init__(self)
        self.statblock.name = "Cavalry"
        self.statblock.attack = 1
        self.statblock.power = 1
        self.statblock.morale = 2
        self.statblock.traits.append(Charge())
        self.costmodifier = 1.5


class Levies(UnitType):
    def __init__(self):
        UnitType.__init__(self)
        self.statblock.name = "Levies"
        self.statblock.morale = -1
        self.costmodifier = 0.75


class Infantry(UnitType):
    def __init__(self):
        UnitType.__init__(self)
        self.statblock.name = "Infantry"
        self.statblock.defense = 1
        self.statblock.toughness = 1


class SiegeEngine(UnitType):
    def __init__(self):
        UnitType.__init__(self)
        self.statblock.name = "Siege Engine"
        self.statblock.attack = 1
        self.statblock.power = 1
        self.statblock.toughness = 1
        self.statblock.costmodifier = 1.5
