from abc import ABCMeta, abstractmethod


class Trait(metaclass=ABCMeta):
    def __init__(self):
        self.name = ""
        self.description = ""
        self.cost = 0

    @abstractmethod
    def try_activate(self):
        pass
    
    
class Amphibious(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Amphibious"
        self.description = "This unit does not suffer terrain penalties for " \
            + "fighting in water or on land."
        self.cost = 50
        
    def try_activate(self):
        pass
    
    
class BredForWar(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Bred for War"
        self.description = "This unit cannot be diminished, and cannot have " \
            + "disadvantage on Morale checks."
        self.cost = 100
        
    def try_activate(self):
        pass
    
    
class Brutal(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Brutal"
        self.description = "This unit inflicts 2 casualties on a successful " \
            + "Power check."
        self.cost = 200
        
    def try_activate(self):
        pass


class Charge(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Charge"
        self.description = "Cannot use while engaged. A Charge is an attack " \
            + "with advantage on the Attack check. It inflicts 2 casualties on " \
            + "a successfulPower check. The charging unit is then engaged with " \
            + "the defending unit and must make a DC 13 Morale check to disengage."

    def try_activate(self):
        pass


class Courageous(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Courageous"
        self.description = "Once per battle, this unit can choose to " \
            + "succeed on a Morale check it just failed."
        self.cost = 50
        
    def try_activate(self):
        pass
    
    
class Eternal(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Eternal"
        self.description = "This unit cannot be horrified, and it always " \
            + "succeeds on Morale checks to attack undead and fiends."
        self.cost = 50
        
    def try_activate(self):
        pass
    
    
class Frenzy(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Frenzy"
        self.description = "If this unit diminishes an enemy unit, it " \
            + "immediately makes a free attack against that unit."
        self.cost = 50
        
    def try_activate(self):
        pass
    
    
class Horrify(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Horrify"
        self.description = "If this unit inflicts a casualty on an enemy " \
            + "unit, that unit must make a DC 15 Morale check. Failure " \
            + "exhausts the unit."
        self.cost = 200
        
    def try_activate(self):
        pass


class Martial(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Martial"
        self.description = "If this unit succeeds on a Power check and its size " \
            + "is greater than the defending unit, it inflicts 2 casualties."
        self.cost = 100

    def try_activate(self):
        pass
    
    
class Mindless(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Mindless"
        self.description = "This unit cannot fail morale checks."
        self.cost = 100

    def try_activate(self):
        pass
    
    
class Regenerate(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Regenerate"
        self.description = "When this unit refreshes, increment its " \
            + "casualty die. This trait ceases to function if the unit " \
            + "suffers a casualty from battle magic."
        self.cost = 200

    def try_activate(self):
        pass
    
    
class Ravenous(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Ravenous"
        self.description = "While any enemy unit is diminished, this unit " \
            + "can spend a round feeding on the corpses to increment their " \
            + "casualty die."
        self.cost = 50

    def try_activate(self):
        pass
    
    
class HurlRocks(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Hurl Rocks"
        self.description = "If this unit succeeds on an Attack check, it " \
            + "inflicts 2 casualties. Against fortifications, it inflicts " \
            + "1d6 casualties."
        self.cost = 250

    def try_activate(self):
        pass
    
    
class Savage(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Savage"
        self.description = "This unit has advantage on the first attack " \
            + "check it makes each battle."
        self.cost = 50

    def try_activate(self):
        pass


class Stalwart(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Stalwart"
        self.description = "Enemy battle magic has disadvantage on Power checks " \
            + "against this unit."
        self.cost = 50

    def try_activate(self):
        pass
    
    
class TwistingRoots(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Twisting Roots"
        self.description = "As an action, this unit can sap the walls of a " \
            + "fortification. Siege units have advantage on Power checks " \
            + "against sapped fortifications."
        self.cost = 200

    def try_activate(self):
        pass
    
    
class Undead(Trait):
    def __init__(self):
        Trait.__init__(self)
        self.name = "Undead"
        self.description = "Green and regular troops must pass a Morale " \
            + "check to attack this unit. Each enemy unit need only do this " \
            + "once."
        self.cost = 50

    def try_activate(self):
        pass