class StatBlock():
    def __init__(self):
        self.name = ""
        self.attack = 0
        self.power = 0
        self.defense = 0
        self.toughness = 0
        self.morale = 0
        self.traits = []

    @classmethod
    def merge(self, *args):
        statblock = StatBlock()
        for arg in args:
            statblock.attack += arg.attack
            statblock.power += arg.power
            statblock.defense += arg.defense
            statblock.toughness += arg.toughness
            statblock.morale += arg.morale
            statblock.traits.extend(arg.traits)

        return statblock


class Unit():
    def __init__(self, name, ancestry, experience, equipment, unittype, size):
        self.name = name
        self.ancestry = ancestry
        self.experience = experience
        self.equipment = equipment
        self.unittype = unittype
        self.size = size
        self.current_size = size

    def description(self):
        return f"{self.ancestry.statblock.name} " \
            + f"{self.experience.statblock.name} " \
            + f"{self.equipment.statblock.name} " \
            + f"{self.unittype.statblock.name}"

    def statblock(self):
        statblock = StatBlock.merge(self.ancestry.statblock,
            self.experience.statblock,
            self.equipment.statblock,
            self.unittype.statblock)
        statblock.defense += 10
        statblock.toughness += 10
        return statblock

    def cost(self, statblock):
        cost = statblock.attack
        cost += statblock.power
        cost += statblock.defense - 10
        cost += statblock.toughness - 10
        cost += statblock.morale*2
        cost *= self.unittype.costmodifier
        cost *= self.size/6
        cost *= 10
        cost += self._get_traits_cost(statblock)
        cost += 30
        return round(cost)

    def _get_traits_cost(self, statblock):
        cost = 0
        for trait in statblock.traits:
            cost += trait.cost
    
        return cost

    def _get_traits_string(self, statblock):
        traits = []
        if statblock.traits:
            for trait in statblock.traits:
                traits.append(f"{trait.name}. {trait.description}")
        else:
            traits.append("None.")

        return "\n\n".join(traits)

    def __str__(self):
        statblock = self.statblock()
        return f"""\
{"="*32}
{self.name}
{"-"*16}
{self.description()}
Cost: {self.cost(statblock)}

Attack: {statblock.attack}\tDefense: {statblock.defense}
Power: {statblock.power}\tToughness: {statblock.toughness}
Morale: {statblock.morale}\tSize: {self.current_size}/{self.size}

Traits
{self._get_traits_string(statblock)}
{"="*32}
"""
