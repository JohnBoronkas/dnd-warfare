from .unit import StatBlock
from .trait import *

class Ancestry():
    def __init__(self):
        self.statblock = StatBlock()


class Bugbear(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Bugbear"
        self.statblock.attack = 2
        self.statblock.morale = 1
        self.statblock.traits.append(Martial())
    
    
class Dragonborn(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Dragonborn"
        self.statblock.attack = 2
        self.statblock.power = 2
        self.statblock.defense = 1
        self.statblock.toughness = 1
        self.statblock.morale = 1
        self.statblock.traits.append(Courageous())


class Dwarf(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Dwarf"
        self.statblock.attack = 3
        self.statblock.power = 1
        self.statblock.defense = 1
        self.statblock.toughness = 1
        self.statblock.morale = 2
        self.statblock.traits.append(Stalwart())
    

class Elf(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Elf"
        self.statblock.attack = 2
        self.statblock.morale = 1
        self.statblock.traits.append(Eternal())
        
        
class ElfWinged(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Elf (Winged)"
        self.statblock.attack = 1
        self.statblock.power = 1
        self.statblock.morale = 1
        self.statblock.traits.append(Eternal())
        
class Ghoul(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Ghoul"
        self.statblock.attack = -1
        self.statblock.defense = 2
        self.statblock.toughness = 2
        self.statblock.traits.append(Undead())
        self.statblock.traits.append(Horrify())
        self.statblock.traits.append(Ravenous())
        

class Gnoll(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Gnoll"
        self.statblock.attack = 2
        self.statblock.morale = 1
        self.statblock.traits.append(Frenzy())
        
        
class Gnome(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Gnome"
        self.statblock.attack = 1
        self.statblock.power = -1
        self.statblock.defense = 1
        self.statblock.toughness = -1
        self.statblock.morale = 1
        
        
class Goblin(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Goblin"
        self.statblock.attack = -1
        self.statblock.power = -1
        self.statblock.defense = 1
        self.statblock.toughness = -1
        
        
class Hobgoblin(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Hobgoblin"
        self.statblock.attack = 2
        self.statblock.morale = 1
        self.statblock.traits.append(BredForWar())
        self.statblock.traits.append(Martial())
        
        
class Human(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Human"
        self.statblock.attack = 2
        self.statblock.morale = 1
        self.statblock.traits.append(Courageous())
        
        
class Kobold(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Kobold"
        self.statblock.attack = -1
        self.statblock.power = -1
        self.statblock.defense = 1
        self.statblock.toughness = -1
        self.statblock.morale = -1
        
        
class Lizardfolk(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Lizardfolk"
        self.statblock.attack = 2
        self.statblock.power = 1
        self.statblock.defense = -1
        self.statblock.toughness = 1
        self.statblock.morale = 1
        self.statblock.traits.append(Amphibious())
        
        
class Ogre(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Ogre"
        self.statblock.power = 2
        self.statblock.toughness = 2
        self.statblock.morale = 1
        self.statblock.traits.append(Brutal())
        
    
class Orc(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Orc"
        self.statblock.attack = 2
        self.statblock.power = 1
        self.statblock.defense = 1
        self.statblock.toughness = 1
        self.statblock.morale = 2
        self.statblock.traits.append(Savage())
        
        
class Skeleton(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Skeleton"
        self.statblock.attack = -2
        self.statblock.power = -1
        self.statblock.defense = 1
        self.statblock.toughness = 1
        self.statblock.morale = 1
        self.statblock.traits.append(Undead())
        self.statblock.traits.append(Mindless())
        
        
class Treant(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Treant"
        self.statblock.power = 2
        self.statblock.toughness = 2
        self.statblock.traits.append(TwistingRoots())
        self.statblock.traits.append(HurlRocks())
        
        
class Troll(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Troll"
        self.statblock.power = 2
        self.statblock.toughness = 2
        self.statblock.traits.append(Regenerate())
        
        
class Zombie(Ancestry):
    def __init__(self):
        Ancestry.__init__(self)
        self.statblock.name = "Zombie"
        self.statblock.attack = -2
        self.statblock.defense = 2
        self.statblock.toughness = 2
        self.statblock.morale = 2
        self.statblock.traits.append(Undead())
        self.statblock.traits.append(Mindless())