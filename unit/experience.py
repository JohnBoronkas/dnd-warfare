from .unit import StatBlock


class Experience():
    def __init__(self):
        self.statblock = StatBlock()


class Green(Experience):
    def __init__(self):
        Experience.__init__(self)
        self.statblock.name = "Green"


class Regular(Experience):
  def __init__(self):
    Experience.__init__(self)
    self.statblock.name = "Regular"
    self.statblock.attack = 1
    self.statblock.toughness = 1
    self.statblock.morale = 1


class Seasoned(Experience):
    def __init__(self):
        Experience.__init__(self)
        self.statblock.name = "Seasoned"
        self.statblock.attack = 1
        self.statblock.toughness = 1
        self.statblock.morale = 2


class Veteran(Experience):
    def __init__(self):
        Experience.__init__(self)
        self.statblock.name = "Veteran"
        self.statblock.attack = 1
        self.statblock.toughness = 1
        self.statblock.morale = 3


class Elite(Experience):
    def __init__(self):
        Experience.__init__(self)
        self.statblock.name = "Elite"
        self.statblock.attack = 2
        self.statblock.toughness = 2
        self.statblock.morale = 4


class SuperElite(Experience):
    def __init__(self):
        Experience.__init__(self)
        self.statblock.name = "Super-Elite"
        self.statblock.attack = 2
        self.statblock.toughness = 2
        self.statblock.morale = 5
