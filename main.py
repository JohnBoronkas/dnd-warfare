# Built with python 3.8.5


from unit.ancestry import *
from unit.experience import *
from unit.equipment import *
from unit.unit import Unit
from unit.unittype import *


# Every size point is about 10 units.
# After battle, a unit with a way to retreat loses about 25%
#     of their lost size and restore 1 size each day until they hit a new 
#     size threshold.
# Units that could not retreat or that were otherwise overwhelmed are
#     eliminated or otherwise captured.


def main():
    guards = Unit(  name="Ogin Guards",
                    ancestry=Orc(),
                    experience=Green(),
                    equipment=Unarmed(),
                    unittype=Levies(),
                    size=1)
    print(guards)
    
    
    guards2 = Unit( name="Ogin Guards",
                    ancestry=Orc(),
                    experience=Green(),
                    equipment=Light(),
                    unittype=Infantry(),
                    size=1)
    print(guards2)
    
    
    goblins = Unit(  name="Goblin Army",
                    ancestry=Goblin(),
                    experience=Green(),
                    equipment=Light(),
                    unittype=Infantry(),
                    size=10)
    print(goblins)

if __name__ == "__main__":
    main()
